import java.io.IOException;
import java.net.*;

public class UdpEchoServer {

    public static void main(final String args[]) {
        new UdpEchoServer();
    }

    static final int BUFFERSIZE = 256;

    public UdpEchoServer() {
        DatagramSocket sock;
        DatagramPacket pack = new DatagramPacket(new byte[BUFFERSIZE], BUFFERSIZE);
        try {
            InetAddress address = InetAddress.getLocalHost();
            sock = new DatagramSocket(4711, address);

            // echo back everything
            while (true) {
                try {
                    sock.receive(pack);
                    System.out.println(pack);
                    System.out.println("Received Package");
                    sock.send(pack);

                    byte[] message = "Nachricht vom Server".getBytes();
                    DatagramPacket packet2 = new DatagramPacket(message, message.length, InetAddress.getLocalHost(),
                            4710);
                    sock.send(packet2);

                } catch (IOException ioe) {
                    System.out.println(ioe);
                }
            }
        } catch (SocketException e) {
            System.out.println(e);
            return;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }
}