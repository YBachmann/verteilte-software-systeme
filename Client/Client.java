import javax.swing.*;
import java.awt.*;
import java.net.*;
import java.util.*;
import java.io.*;
import javax.swing.*;

class Main {

    public static void main(final String args[]) {
        new Client();
        // new UdpEchoServer();
    }
}

class Client {

    View view;

    private DatagramSocket dsocket;
    static final int BUFFERSIZE = 256;

    public Client() {
        view = new View(this);
        createSocket();

        Thread listen = new Thread() {
            public void run() {
                try {
                    receiveMessage();
                } catch (Exception v) {
                    System.out.println(v);
                }
            }
        };

        listen.start();

    }

    private void createSocket() {
        try {
            dsocket = new DatagramSocket(4710, InetAddress.getLocalHost());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String m) {
        try {
            System.out.println("Sending Message: " + m);
            int port = 4711;

            byte[] message = m.getBytes();

            // Get the internet address of the specified host
            InetAddress address = InetAddress.getLocalHost();

            // Initialize a datagram packet with data and address
            DatagramPacket packet = new DatagramPacket(message, message.length, address, port);

            // Create a datagram socket, send the packet through it, close it.

            dsocket.send(packet);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private void receiveMessage() {
        DatagramPacket pack = new DatagramPacket(new byte[BUFFERSIZE], BUFFERSIZE);
        while (true) {
            try {
                dsocket.receive(pack);
                String receivedStr = new String(pack.getData(), 0, pack.getLength());
                System.out.println("Client Received Package: " + receivedStr);
                view.addMessageToTextArea("Server: " + receivedStr);
            } catch (IOException ioe) {
                System.out.println(ioe);
            }
        }
    }

    private void closeSocket(DatagramSocket dsocket) {
        dsocket.close();
    }
}
