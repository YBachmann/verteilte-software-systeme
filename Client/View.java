import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View implements ActionListener {

    Client client;

    private JFrame frame;
    private JButton send;
    private JTextField tf;
    private JTextArea ta;

    public View(Client client) {
        this.client = client;

        createView();
    }

    private void createView() {
        // Creating the Frame
        frame = new JFrame("Chat Frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 700);

        // Creating the MenuBar and adding components
        final JMenuBar mb = new JMenuBar();
        final JMenu m1 = new JMenu("FILE");
        final JMenu m2 = new JMenu("Help");
        mb.add(m1);
        mb.add(m2);
        final JMenuItem m11 = new JMenuItem("Open");
        final JMenuItem m22 = new JMenuItem("Save as");
        m1.add(m11);
        m1.add(m22);

        // Erstellung Array vom Datentyp Object, Hinzufügen der Komponenten
        JTextField serverAdresse = new JTextField();
        JTextField benutzername = new JTextField();
        Object[] message = { "Server-Adresse", serverAdresse, "Benutzername", benutzername };

        JOptionPane pane = new JOptionPane(message, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
        pane.createDialog(null, "Login").setVisible(true);

        // Creating the panel at bottom and adding components
        final JPanel panel = new JPanel(); // the panel is not visible in output
        final JLabel label = new JLabel("Enter Text");
        tf = new JTextField(10); // accepts upto 10 characters
        send = new JButton("Send");
        send.addActionListener(this);
        panel.add(label); // Components Added using Flow Layout
        panel.add(tf);
        panel.add(send);

        // Text Area at the Center
        ta = new JTextArea();

        // Adding Components to the frame.
        frame.getContentPane().add(BorderLayout.SOUTH, panel);
        frame.getContentPane().add(BorderLayout.NORTH, mb);
        frame.getContentPane().add(BorderLayout.CENTER, ta);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == this.send) {
            client.sendMessage(tf.getText());
            addMessageToTextArea("Client: " + tf.getText());
            frame.repaint();
        }
    }

    public void addMessageToTextArea(String text) {
        ta.setText(ta.getText() + text + "\n");
    }

}